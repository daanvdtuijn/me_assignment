# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 17:32:43 2020

@author: jensb

This file was just a playground in Python. 
Results and plots used in the report are made in R.

"""
import pandas as pd 
import numpy as np
import statsmodels.graphics.tsaplots as graph

import arch
from arch.unitroot import ADF

import statsmodels.stats.diagnostic as diag

#import arch.unitroot as unitrootF
from arch.unitroot import PhillipsPerron

df = pd.read_excel('../data/MVE_assignment_2020_dataset.xlsx')

# drop irrelevant columns and set index
df.drop(['ISO_N3', 'ISO_C3'], axis=1, inplace=True)
df = df.set_index(['year','cntry.name']).stack().unstack([1,2])

# clean string values 
df = df.apply(lambda x: pd.to_numeric(x, errors='coerce')).astype(float)
countries = ['Norway', 'Thailand']

raw_data = df.copy()

df2 = df.copy()
for c in countries:
    df2[c, 'mean_pre']       = df2[c, 'mean_pre'] - np.mean(df2[c, 'mean_pre'])
    df2[c, 'mean_rad']       = df2[c, 'mean_rad'] - np.mean(df2[c, 'mean_rad'])
    df2[c, 'mean_tmp']       = df2[c, 'mean_tmp'] - np.mean(df2[c, 'mean_tmp'])
    df2[c, 'NY.GDP.MKTP.KD'] = np.log(df2[c, 'NY.GDP.MKTP.KD']).diff()
    df2[c, 'NY.GDP.PCAP.KD'] = np.log(df2[c, 'NY.GDP.PCAP.KD']).diff()
    df2[c, 'SP.POP.TOTL']    = df2[c, 'SP.POP.TOTL'].diff().diff()
    df2[c, 'AG.LND.AGRI.K2'] = df2[c, 'AG.LND.AGRI.K2'].diff()
    df2[c, 'AG.PRD.CROP.XD'] = df2[c, 'AG.PRD.CROP.XD'].diff()

transformed_data = df2

print(transformed_data)


############################ testing ##############################



# 1. Determining order of integration 

#Idea: use DF test to find whether time series is I(2), I(1) or I(0). 
#DF tests whether it contains a unit root. Once p-value is lower than 0.05,
#this can be rejected. E.g. GDP shows a significant alpha when differenced, so it is I(1).
#Some variables might already be significant without differencing so they are I(0).
#For the population data we for example find it is indeed I(2) as it only shows significance after taking the 2nd difference

# Problems determining order of integration:
# - Choice of deterministic components as they have to be provided in the Dickey Fuller function, seperately for every variable
# - Approach to determine integrated order: start with raw data of variable and then continue until you find significance? 
# - So, do we simply skip the transformed data and use raw data for testing?


# 2. Determine serial correlation in residuals of Dickey Fuller regression

# - based on test - Residuals of regressions can be captured and tested for correlation by ljunbox test
# - visually - Residuals of regressions can be captured and plotted and then we can check the autocorrelation ourselves.


# 3. Extend the testing of unit roots, taking into account the serial correlations found in part (2).
# - use augmented dickey fuller or/and philips-perron
# idea : if these tests are also rejected at significant level, there is no unit root

# Question to answer here: does the number of lags you use influence the findings?




############### example 1. Determining order of integration ################

#approach1:
#for every variable and every country:
#test raw data on non-deterministic trends, constant only, constant + linear trend + differenced raw data

GDP_data_raw = raw_data[('Norway', 'NY.GDP.MKTP.KD')]

print("=== GDP raw data ===")
print(ADF(GDP_data_raw, lags=0, trend = 'n').pvalue)
print(ADF(GDP_data_raw, lags=0).pvalue)
print(ADF(GDP_data_raw, lags=0, trend='ct').pvalue)
print(ADF(np.diff(GDP_data_raw), lags=0).pvalue)


#approach2?:
#test transformed data on dickey fuller

GDP_data_transformed = transformed_data[('Norway', 'NY.GDP.MKTP.KD')].dropna()

print("=== GDP transformed data ===")
print(ADF(GDP_data_transformed, lags=0, trend = 'n').pvalue)
print(ADF(GDP_data_transformed, lags=0).pvalue)
print(ADF(GDP_data_transformed, lags=0, trend='ct').pvalue)
print(ADF(np.diff(GDP_data_transformed), lags=0).pvalue)

#check population total, as we took the second difference here
population_data_raw = raw_data[('Norway', 'SP.POP.TOTL')]

print("=== Population data ===")
print(ADF(population_data_raw, lags=0, trend = 'n').pvalue)
print(ADF(population_data_raw, lags=0).pvalue)
print(ADF(population_data_raw, lags=0, trend='ct').pvalue)
print(ADF(np.diff(population_data_raw), lags=0).pvalue)
print(ADF(np.diff(np.diff(population_data_raw)), lags=0).pvalue)


############### example 2. Determine serial correlation in residuals of Dickey Fuller regression ################

residuals = ADF(np.diff(GDP_data_raw), lags=0).regression.resid

#test using ljungbox?
lbstat, pvalue = diag.acorr_ljungbox(residuals, lags = 20)

#or just plot?
graph.plot_acf((residuals), lags = 20)

############# example 3 - Extend the testing of unit roots, taking into account the serial correlations found in part (2). ######33

GDP_pp = PhillipsPerron(GDP_data_transformed, lags = 20, trend ="ct")
print('GDP PhilipsPerron:', GDP_pp.pvalue)

