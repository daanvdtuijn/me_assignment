import pandas as pd
import numpy as np
import statsmodels.api as sm
from statsmodels.tsa.tsatools import add_trend
import matplotlib.pyplot as plt
from scipy import stats

from arch.unitroot.cointegration import engle_granger, phillips_ouliaris, DynamicOLS, FullyModifiedOLS
from statsmodels.tsa.vector_ar.vecm import VECM, coint_johansen, select_coint_rank, select_order
from statsmodels.tsa.stattools import adfuller

# define variables
countries = ['Norway', 
             'Thailand']

variables = ['GDP Per Capita', 
             'Agricultural Land (sq. km)', 
             'Average Precipitation',
             'Average Temperature']

iterables = [countries, variables]
midx      = pd.MultiIndex.from_product(iterables)

# import data
df = pd.read_pickle('../data/df_levels.pkl')[midx]

df.rename(columns = {'GDP Per Capita'             : 'GDP',
                     'Agricultural Land (sq. km)' : 'AGR',
                     'Average Precipitation'      : 'PREC',
                     'Average Temperature'        : 'TEMP'},
                     inplace=True)

df = df.dropna()

# # USE TRANSFORMED DATA
midx = pd.MultiIndex.from_product([countries, ['GDP', 'AGR']])
df[midx] = np.log(df[midx])

########################
# RESIDUAL BASED TESTS #
########################
residual_tests = dict()

for country in countries:

    df1 = df[country].copy()
    Y = df1['GDP']
    X = df1[['AGR', 'PREC', 'TEMP']]

    for trend in ['n', 'c', 'ct']:

        test = engle_granger(Y, X, trend, lags=3)
        residual_tests[country, trend, 'EG'] = test
        print(f'EG {country} with "{trend}"')
        print(f'test-statistic: \t{test.stat:.2f} \ncritical value at 5%: \t{test.critical_values.loc[5]:.2f}\n\n')
        
    for trend in ['n', 'c', 'ct']:

        test = phillips_ouliaris(Y, X, trend, test_type='Zt')
        residual_tests[country, trend, 'PO'] = test
        print(f'PO {country} with "{trend}"')
        print(f'test-statistic: \t{test.stat:.2f} \ncritical value at 5%: \t{test.critical_values.loc[5]:.2f}\n\n')

##################################
# SINGLE COINT VECTOR ESTIMATION #
##################################
# CHANGE COUNTRY AND RUN SCRIPT BELOW until ME based tests

country = 'Thailand'
df1 = df[country].copy()

# residuals based
y = df1['GDP']
X = df1[['AGR', 'PREC', 'TEMP']]

# SOLS, DOLS, FMOLS, 
sls   = sm.OLS(y, add_trend(X, 'ct') ).fit()
dols  = DynamicOLS(y, X, 'ct', lags=3, leads=-1).fit()
fmols = FullyModifiedOLS(y, X, trend='ct', x_trend='ct').fit()

# CECM; use difference = 'd'; and lags = 'l'
dy = pd.DataFrame(y).diff().add_prefix('d') # take first orders for GDP
dX = X.diff().add_prefix('d')               # same for other variables

lX   = X.shift().add_prefix('l1')
l1y  = pd.DataFrame(y).shift(1).add_prefix('l1')

l1dy = pd.DataFrame(dy).shift(1).add_prefix('l1')
l2dy = pd.DataFrame(dy).shift(2).add_prefix('l2')
l3dy = pd.DataFrame(dy).shift(3).add_prefix('l3')

l1dX = dX.shift(1).add_prefix('l1')
l2dX = dX.shift(2).add_prefix('l2')
l3dX = dX.shift(3).add_prefix('l3')

# k=3 lags of itself to stay consistent
dfs    = [lX, dX, l1dy, l1dX, l2dy, l2dX]#, l3dy, l3dX]
X_cecm = l1y.join(dfs) 
cecm   = sm.OLS(dy[4:], add_trend(X_cecm[4:], 'ct', prepend=True)).fit()
print(f'CECM original estimation: {cecm.summary()}')

H_null  = 0
t_value = (cecm.params[2] - H_null) / cecm.bse[2]
p_value = stats.t.cdf(t_value, df=cecm.df_resid)
print(f'\n\nlambda_1 point estimate: {cecm.params[2]:.4f}')
print(f'\np-value from no-cointegration test using H0: speed of adjustment coef = 0: {p_value:.4f}')

params  = cecm.params
          # cons, trend, agr, prec, temp
psi     = [params[0], params[1], params[3], params[4], params[5]]
lambda1 =  params[2]
gamma   = np.round(psi / lambda1, 3) * -1

se       = cecm.HC0_se
psi_se   = [se[0], se[1], se[3], se[4], se[5]]
lmbd_se  =  se[2]
gamma_se = np.round(psi_se / lmbd_se, 3)

# for latex output
ar = [gamma[0], gamma_se[0], 
      gamma[1], gamma_se[1],
      gamma[2], gamma_se[2],
      gamma[3], gamma_se[3],
      gamma[4], gamma_se[4]] 

# for latex output
[print(i) if idx%2==0 else print(f'({i})') for idx, i in enumerate(list(ar))];

# PLOT RESIDUALS
methods = ['SLS', 'DOLS', 'CECM', 'FMLS']
df_resid_NOR = pd.read_pickle('resid_NOR.pkl')
df_resid_THA = pd.read_pickle('resid_THA.pkl')

fig, ax = plt.subplots(1,2, figsize=(20,4))

[ax[0].plot(df_resid_NOR[c], label=f'{c}') for c in methods];
ax[0].axhline(0, c='black', ls='-', lw=0.3)
ax[0].set_title('Norway')
ax[0].legend()

[ax[1].plot(df_resid_THA[c], label=f'{c}') for c in methods];
ax[1].axhline(0, c='black', ls='-', lw=0.3)
ax[1].set_title('Thailand')

fig.savefig('images/residual_plot.png')

# TESTING RESIDUALS
df_print = pd.DataFrame(index=countries, columns=methods)

# constant/trend not included because residuals shouldn't have these

# ADF test for latex
for i in methods:
    df_print.loc['Norway', i]   = adfuller(df_resid_NOR[i].dropna(), maxlag=3)[1] # pval
    df_print.loc['Thailand', i] = adfuller(df_resid_THA[i].dropna(), maxlag=3)[1] # pval
print(df_print.to_latex())

##################
# ME BASED TESTS #
##################

# maximum likelihood based
for country in countries:
    df1 = df[country].copy()
    test = coint_johansen(df1, det_order=1, k_ar_diff=3)
    print(f'{country} - trace stats: {np.round(test.trace_stat, 2)}')
    print(f'{country} - me stats:    {np.round(test.max_eig_stat, 2)}')

# critical values are obtained using object
# e.g.

print(f'\ncrit vals of ME test: \n\n{test.max_eig_stat_crit_vals}')

######################## 
# VECM JOHANSEN SYSTEM #
########################

# constant and trend
ct = np.concatenate((np.ones(len(x))[np.newaxis].T, 
                     np.arange(len(x))[np.newaxis].T), axis=1)

x = df['Norway'].reset_index(drop=True)
modeln = VECM(endog=x, coint_rank=1, k_ar_diff=3, deterministic='cili').fit()
modeln.summary()

x = df['Thailand'].reset_index(drop=True)
modelt = VECM(endog=x, coint_rank=1, k_ar_diff=3, deterministic='cili').fit()
modelt.summary()


######################################
######################################
# ME BASED TESTS ## using i(1) only! #
######################################

midx  = pd.MultiIndex.from_product([countries, ['GDP', 'AGR']]) # non-stationary variables
df_ns = df[midx]

# maximum likelihood based
for country in countries:
    df1 = df_ns[country].copy()
    test = coint_johansen(df1, det_order=1, k_ar_diff=3)
    print(f'{country} - trace stats: {np.round(test.trace_stat, 2)}')
    print(f'{country} - me stats:    {np.round(test.max_eig_stat, 2)}')

######################## 
# VECM JOHANSEN SYSTEM #
########################

# # constant and trend
# ct = np.concatenate((np.ones(len(x))[np.newaxis].T, 
#                      np.arange(len(x))[np.newaxis].T), axis=1)

x = df_ns['Norway'].reset_index(drop=True)
modeln = VECM(endog=x, coint_rank=1, k_ar_diff=3, deterministic='cili').fit()
modeln.summary()

x = df_ns['Thailand'].reset_index(drop=True)
modelt = VECM(endog=x, coint_rank=1, k_ar_diff=3, deterministic='cili').fit()
modelt.summary()

df_b = pd.DataFrame(columns=midx, 
                    index=['beta_i1', 'p1']).fillna(0)



# ALL COMMENTED CODE BELOW IS FOR LATEX OUTPUT
# alphas
# a = np.round(np.array([modeln.const_coint[0][0], modeln.lin_trend_coint[0][0], modeln.beta[0][0], modeln.beta[1][0]]), 3)
# b = np.round(np.array([modeln.pvalues_det_coef_coint[:,0][0], modeln.pvalues_det_coef_coint[:,0][1], modeln.pvalues_beta[0][0], modeln.pvalues_beta[1][0]]), 3)
# at = np.round(np.array([modelt.const_coint[0][0], modelt.lin_trend_coint[0][0], modelt.beta[0][0], modelt.beta[1][0]]), 3)
# bt = np.round(np.array([modelt.pvalues_det_coef_coint[:,0][0], modelt.pvalues_det_coef_coint[:,0][1], modelt.pvalues_beta[0][0], modelt.pvalues_beta[1][0]]), 3)

# df_b.iloc[0] = np.concatenate([a,at])
# df_b.iloc[1] = np.concatenate([b,bt])


# midx = pd.MultiIndex.from_product([countries, ['GDP', 'AGR', 'PREC', 'TEMP']])
# df_a = pd.DataFrame(columns=midx, index=['alpha_i1', 'p1', 'alpha_i2', 'p2']).fillna(0)

# df_a.iloc[0] = np.round(np.append(modeln.alpha[:,0], modelt.alpha[:,0]),3)
# df_a.iloc[1] = np.round(np.append(modeln.pvalues_alpha[:,0], modelt.pvalues_alpha[:,0]),2)
# df_a['Thailand'].iloc[2] = np.round(modelt.alpha[:,1], 3)
# df_a['Thailand'].iloc[3] = np.round(modelt.pvalues_alpha[:,1], 2)

# # betas
# midx = pd.MultiIndex.from_product([countries, ['cons', 'trend', 'GDP', 'AGR', 'PREC', 'TEMP']])
# df_b = pd.DataFrame(columns=midx, index=['beta_i1', 'p1', 'beta_i2', 'p2']).fillna(0)

# cn = modeln.const_coint[:,0]
# tn = modeln.lin_trend_coint[:,0]
# pn = np.squeeze(modeln.pvalues_det_coef_coint)

# ct1 = modelt.const_coint[:,0]
# tt1 = modelt.lin_trend_coint[:,0]
# pt1 = modelt.pvalues_det_coef_coint[:,0]

# ct2 = modelt.const_coint[:,1]
# tt2 = modelt.lin_trend_coint[:,1]
# pt2 = modelt.pvalues_det_coef_coint[:,1]

# cn  = np.append(np.array([cn, tn]), modeln.beta[:,0])
# ct1 = np.append(np.array([ct1, tt1]), modelt.beta[:,0])
# df_b.iloc[0] = np.round(np.append(cn,ct1),3)

# pn = np.append(pn, modeln.pvalues_beta[:,0])
# pt1 = np.append(pt1, modelt.pvalues_beta[:,0])
# df_b.iloc[1] = np.round(np.append(pn, pt1),2)

# t2 = np.append(ct2, tt2)
# df_b['Thailand'].iloc[2] = np.round(np.append(t2, modelt.beta[:,1]), 3)
# df_b['Thailand'].iloc[3] = np.round(np.append(pt2, modelt.pvalues_beta[:,1]), 2)


# for latex, for sls, dols, fmols
# def correct_order(test):

#     coef = np.round(test.params , 3)
#     std  = np.round(test.std_errors,3)

#     # c, trend, agr, prec, temp
#     return [coef[3], std[3], 
#             coef[4], std[4], 
#             coef[0], std[0],
#             coef[1], std[1],
#             coef[2], std[2]]

# def formatter(test):

#     ar = correct_order(test)

#     [print(i) if idx%2==0 else print(f'({i})') for idx, i in enumerate(list(ar))]