import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf

# read data
df = pd.read_excel('../data/MVE_assignment_2020_dataset.xlsx')

# drop irrelevant columns and set index
df.drop(['ISO_N3', 'ISO_C3'], axis=1, inplace=True)
df = df.set_index(['year','cntry.name']).stack().unstack([1,2])

# clean string values 
df = df.apply(lambda x: pd.to_numeric(x, errors='coerce')).astype(float)

# for table formatting
def human_format(num):
    num = float('{:.3g}'.format(num))
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    return '{}{}'.format('{:f}'.format(num).rstrip('0').rstrip('.'), ['', 'K', 'M', 'B', 'T'][magnitude])

### function for plots
def plot_variables(df, countries):

    #variables = list(df.columns.get_level_values(1).drop_duplicates())
    variables = ['Average Precipitation', 'Average Temperature', 
                 'GDP Per Capita', 'Agricultural Land (sq. km)']

    fig, axs = plt.subplots(2, 4, figsize=(20,6), sharex=True)
    #fig.suptitle(title, fontsize=17)

    # country names
    plt.figtext(0.24, 0.95, countries[0], size=16)
    plt.figtext(0.74, 0.95, countries[1], size=16)

    color   = 'lightseagreen'
    country = countries[0]

    for i, (ax, var) in enumerate(zip(axs.flatten('F'), 2*variables)): 

        if i == len(variables):
            color  = 'indianred'
            country = countries[1]

        ax.set_title(f'{var}')
        ax.plot(df.index, df[country, var], c=color)


        # ticks = ax.get_yticks()
        # if not all([tick.is_integer() for tick in ticks]):
        #     ax.set_yticklabels( [human_format(int(x)) for x in ax.get_yticks().tolist()])
            
    fig.tight_layout()
    fig.subplots_adjust(top=0.92)

    return fig

### function for autocorrelation function plots
def plot_acfs(df, countries, partial=False):
    
    variables = list(df.columns.get_level_values(1).drop_duplicates())
    mcols = pd.MultiIndex.from_product([countries, variables])

    fig, axs = plt.subplots(4, 4, figsize=(20,12), sharex=True, sharey=True)
    #fig.suptitle(title, fontsize=17)

    # country names
    plt.figtext(0.25, 0.94, countries[0], size=15)
    plt.figtext(0.74, 0.94, countries[1], size=15)

    for ax, mcol in zip(axs.flatten('F'), mcols): 
        for c in countries:

            if not partial:
                plot_acf(df[mcol].dropna(), ax=ax, lags=20)
            if partial:
                plot_pacf(df[mcol].dropna(), ax=ax, lags=20)

            ax.set_title(f'{mcol[1]}')
        
    fig.tight_layout()
    fig.subplots_adjust(top=0.90)

    return fig

# map col names for plot
mapper = {
    "mean_pre"      : "Average Precipitation",   
    "mean_rad"      : "Average Radiation", 
    "mean_tmp"      : "Average Temperature", 
    "NY.GDP.MKTP.KD": "GDP", 
    "NY.GDP.PCAP.KD": "GDP Per Capita", 
    "SP.POP.TOTL"   : "Total Population", 
    "AG.LND.AGRI.K2": "Agricultural Land (sq. km)", 
    "AG.PRD.CROP.XD": "Crop Production Index"
    }

df1 = df.copy().rename(columns=mapper)
countries = ['Norway', 'Thailand']

# plot before transformation
fig = plot_variables(df1, countries=countries)
fig.savefig('images/plots_before_trans')

##################
# transformation #
##################
df2 = df.copy()
for c in countries:
    df2[c, 'mean_pre']       = df2[c, 'mean_pre'] - np.mean(df2[c, 'mean_pre'])
    df2[c, 'mean_rad']       = df2[c, 'mean_rad'] - np.mean(df2[c, 'mean_rad'])
    df2[c, 'mean_tmp']       = df2[c, 'mean_tmp'] - np.mean(df2[c, 'mean_tmp'])
    df2[c, 'NY.GDP.MKTP.KD'] = np.log(df2[c, 'NY.GDP.MKTP.KD']).diff()
    df2[c, 'NY.GDP.PCAP.KD'] = np.log(df2[c, 'NY.GDP.PCAP.KD']).diff()
    df2[c, 'SP.POP.TOTL']    = df2[c, 'SP.POP.TOTL'].diff().diff()
    df2[c, 'AG.LND.AGRI.K2'] = df2[c, 'AG.LND.AGRI.K2'].diff()
    df2[c, 'AG.PRD.CROP.XD'] = df2[c, 'AG.PRD.CROP.XD'].diff()

# map col names
mapper = {
    "mean_pre"      : "Average Precipitation (demeaned)",   
    "mean_rad"      : "Average Radiation (demeaned)", 
    "mean_tmp"      : "Average Temperature (demeaned)", 
    "NY.GDP.MKTP.KD": "Growth Rate GDP (log)", 
    "NY.GDP.PCAP.KD": "Growth Rate GDP Per Capita (log)", 
    "SP.POP.TOTL"   : "Growth Rate Total Population", 
    "AG.LND.AGRI.K2": "Growth Rate Agricultural Land (sq. km)", 
    "AG.PRD.CROP.XD": "Growth Rate Crop Production Index"
    }

df2 = df2.rename(columns=mapper)

# plot after transformation
fig = plot_variables(df2, countries=countries)
fig.savefig('images/plots_after_trans.png')

# acf plot after transformation
fig = plot_acfs(df2, countries=countries)
fig.savefig('images/acf_after_trans.png')